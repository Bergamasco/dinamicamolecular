#!/bin/bash
# Bash script for running njobs time the same physics and saving each run in a directory

change_temp()
{
awk  -v tt=$1 'NR!=5 {print $0} NR==5 {$1=tt;print $0}' input.dat > out
mv out input.dat

}

for temp in 0.7 0.75 0.8 0.85 0.9 0.95 1.0 1.05 1.10 1.15 1.20 1.25 1.30 1.35 1.40 
do
    dir=${temp}_temp
    if [ ! -e $dir ] ; then
        mkdir $dir
    fi

    cp input.dat send_job.sh seed.dat $dir
    cd $dir
    change_temp $temp
    touch RUN_1
    touch promedios.dat
    ./send_job.sh # acá va el ejecutable

    cp seed.dat ../seed.dat

    cd ../
done
