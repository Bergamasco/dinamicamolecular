#!/bin/bash
# Bash script for running njobs time the same physics and saving each run in a directory

change_dens()
{
awk  -v tt=$1 'NR!=4 {print $0} NR==4 {$1=tt;print $0}' input.dat > out
mv out input.dat

}

for dens in 0.001 0.01 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 0.55 0.6 0.65 0.7 0.75 0.8 0.9 1.0 
do
    dir=${dens}_dens
    if [ ! -e $dir ] ; then
        mkdir $dir
    fi

    cp input.dat send_job.sh seed.dat $dir
    cd $dir
    change_dens $dens
    touch RUN_1
    touch promedios.dat
    ./send_job.sh # acá va el ejecutable

    cp seed.dat ../seed.dat

    cd ../
done
