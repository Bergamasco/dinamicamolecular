#!/bin/bash

	cd $HOME/SimComp/Dinamica/1.1_Temp
	
	echo 'Analizando: ' 

	for dens in 0.001 0.01 0.1 0.8 0.9 1.0
	do
		cd ${dens}_dens
		#CALCULO EL PROMEDIO DE LAS ENERGIAS Y ENERGIAS^2 CON SUS RESPCTIVOS DESVIOS ESTANDAR
		awk -v dens=$dens 'NR>1 {
			sumP+=$1; 
			sumPP+=$2; 
			sumP2+=$1*$1; 
			sumPP2+=$2*$2;
			
		} END {
			term = NR-1;
			print dens, sumP/term, sqrt(sumP2/term-(sumP/term)^2), sumPP/term, sqrt(sumPP2/term-(sumPP/term)^2)
		
		}' promedios.dat >> ../TvsDensidad.dat
	
		cd ../

	done
	
