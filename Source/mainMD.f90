program mainMD
  USE ziggurat
  USE rutinasMD
  IMPLICIT NONE
  LOGICAL :: es
  INTEGER (KIND = 4) :: i, j, k
  REAL(KIND=8) :: r_aux, vb, nid
  CHARACTER(LEN=*), PARAMETER :: FILE_INPUT = "input.dat"


  !LEO EL ARCHIVO input.dat para optener los parametros del sistema
  inquire(file=FILE_INPUT, exist=es)
  IF(es) THEN
    open(unit=10, file=FILE_INPUT, status="old")
    read(10,*) Pasos
    read(10,*) N
    read(10,*) dt_MD
    read(10,*) rho
    read(10,*) Temp
    read(10,*) epsil, sigma
    read(10,*) rc
    read(10,*) nhis
    close(10)
  else
    WRITE(6,*) "ERROR!!!. NO EXISTE ARCHIVO input.dat"
  End if
  rc = rc * sigma
  frecMed = 250
  L = (dfloat(N) / rho)**(1.0D0/3.0D0)
  pi = dacos(-1.0D0)

  !*******Impresiones por pantalla ***************************************
  print *, "Parametros del Sistema"
  print *, "**********************************************************************"
  print *, "Numero de Particulas: ", N, "|Tamaño Caja:", L, "|Temperatura:", Temp
  print *, "Potencial--> ", "Sigma:", sigma, "Epsilon:", epsil
  print *, "**********************************************************************"

  !Leo semilla
  inquire(file="seed.dat", exist=es)
  IF(es) THEN
    open(unit=10, file="seed.dat", status="old")
    read(10,*) seed
    close(10)
    print *, "Leyendo semilla para numeros aleatorios"
  else
    seed = 24583490
  End if
  call zigset(seed)
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ALLOCATE( r(Dim, N), v(Dim, N), f(Dim, N), v_cm(dim), g(0:nhis) )  !Creo los vectores que almacenan las variables r , v y f

  g = 0.0D0
  ngr = 0
  delg = L / (2.0D0 * nhis)


  open(10, file=instDatos)
  write(10,*)
  close(10)

  !inicializo a cero las variables y acumuladores
  r = 0.0D0
  v = 0.0D0
  f = 0.0D0
  v_cm = 0.0D0
  PromP = 0.0D0
  PromP2 = 0.0D0
  PromT = 0.0D0
  PromT2 = 0.0D0

  !Defino las posiciones y velocidades Iniciales
  call PosIni()
  call VelocIni()
  call EnergiaCinetica()

  !%%%%%%%%%%%%%%-INICIALIZACION G(r) - %%%%%%%%%%%%%%%%%%
  ngr = 0
  delg = L / (2.0D0 * nhis)
  g = 0.0D0
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55

  Call EscrituraVTF("dinamica.vtf", 0) !Inicializo el archivo VTF
  Call EscrituraVTF("dinamica.vtf", 1) !Gravo la posicion inicial
  call FUERZAS()
  !Bucle de Dinamica
  print *, "Empezando Dinamica"
  do i=1,Pasos

      call Velocity_Verlet()

      if (mod(i, frecMed) .eq. 0) THEN
        call Medir()
        call EscrituraVTF("dinamica.vtf", 1)
        print *, i, EnPot/float(N), EnCin/float(N), EnTot/float(N), Tmed, Pmed
      end if

  end do

  !%%%%%%%%%%%%-Acondicionamiento y Normalizacion de la G(r) -%%%%%%%%
  open(10, file="gr.dat")
  do i=1,nhis
    r_aux = delg * (i + 0.5D0)
    vb = ((i+1)**3 - i**3) * delg**3
    nid = (4.0D0/3.0D0) * pi*vb*rho
    g(i) = g(i) / (ngr*N*nid)
    write(10,*) r_aux, g(i)
  end do
  close(10)

  open(10, file="r.dat")
  write(10,*) r
  close(10)

  open(10, file="v.dat")
  write(10,*) v
  close(10)


  open(10, file=promDatos, status="old", position="append", action="write")
  write(10,*) PromT/dfloat(pasos), PromT2/dfloat(pasos), PromP/dfloat(pasos), PromP2/dfloat(pasos)
  close(10)



  print*, "*************** Posiciones ********************"
  do i=1,10
    write(6,*) V(:,i)
  end do
  print*, "********************************************"
  print*, "Energia Potencia: " , EnPot/float(N) , "Potencial(rc):", pot_rc/float(N)
  print*, "Energia Cinetica: " , EnCin/float(N)
  print*, "Energia Total:    " , (EnPot + EnCin)/float(N)
  print*, "*************** FUERZAS ********************"
  do i=1,N
!    write(6,*) f(:,i)
  end do
  print*, "********************************************"
  !**********************************************************************

!Escribe la ultima semilla
  open(unit=10,file='seed.dat',status='unknown')
  seed = shr3()
  write(10,*) seed
  close(10)

end program mainMD
