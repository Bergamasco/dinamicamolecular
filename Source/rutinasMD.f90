module rutinasMD
  USE ziggurat
  INTEGER (KIND = 4) :: N, seed, Dim = 3, pasos, frecMed, ngr, ig, nhis
  REAL (kind = 8), dimension(:,:), allocatable :: r, v, f
  REAL (kind = 8), dimension(:), allocatable :: v_cm, g
  REAL (kind = 8) :: epsil, sigma, L, Temp, EnPot, EnCin, EnTot, rc, pot_rc, masa = 1.0D0, dt_MD, kb=1.0D0
  REAL (kind = 8) :: Pmed, Pv, Tmed, rho, gamma = 0.5d0, delg, pi, promP, PromP2, PromT, PromT2
  CHARACTER(len=50):: promDatos = "promedios.dat"
  CHARACTER(len=50):: instDatos = "instantaneos.dat"
contains


subroutine EnergiaCinetica()
    integer (kind = 4) :: i, j, k

    EnCin =0.0D0
    do j=lbound(v, dim=2),ubound(v, dim=2)
      do i=lbound(v, dim=1), ubound(v, dim=1)
        EnCin = Encin + 0.5D0 * masa * v(i,j)**2
      end do
    end do
    return
end subroutine EnergiaCinetica

subroutine Fuerzas()
    implicit none
    real (kind = 8) :: dist, pares, f_aux
    real (kind = 8), dimension(:), allocatable :: dr
    integer (kind = 4) :: i, j, k, row_r_max, row_r_min, col_r_max, col_r_min

    EnPot =0.0D0
    Pv = 0.0D0

    pot_rc = 4*epsil*((sigma/rc)**12 - (sigma/rc)**6)

    col_r_min = lbound(r, dim=2)
    col_r_max = ubound(r, dim=2)

    row_r_min = lbound(r, dim=1)
    row_r_max = ubound(r, dim=1)

    allocate(dr(row_r_min:row_r_max))
    dist = 0.0D0
    dr = 0.0
    f = 0
    pares = 0.0d0
    ngr = ngr + 1 !Variable para la G(r)
    do j=col_r_min,col_r_max-1
      do k = j+1, col_r_max
        dist = 0.0D0

        !CALCULO LA DISTANCIA A LA PARTICULA TENIENDO EN CUENTA
        !LA PERIODICIDAD DEL SISTEMA
        do i=row_r_min,row_r_max
          dr(i) = (r(i,k) - r(i,j))
          dr(i) = dr(i) - L*nint(dr(i)/L)
          dist = dist + dr(i)**2
        end do

        dist = dsqrt(dist)
        !print *, dist
        !%%%%%%%%%%%5- Calculo de la G(r) - %%%%%%%%%%%%
        if (dist .LT. L/2.0D0) THEN
            ig = int(dist/delg)
            g(ig) = g(ig) + 2
        end if
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        !%%%%%%- Calculo Fuerza - %%%%%%%%%%%%%%%%%%%%%%
        if (dist .le. rc) then
          do i=row_r_min,row_r_max
            pares = pares + 1.0D0
            f_aux = 4 * (epsil/sigma**2) * ( 12*(sigma/dist)**14 - 6*(sigma/dist)**8 ) * dr(i)
            f(i, j) = f(i,j) - f_aux
            f(i, k) = f(i,k) + f_aux
            Pv = Pv + rho/dfloat(N*dim) * f_aux * dr(i) !Termino del virial de la presion
          end do
          EnPot = EnPot + 4*epsil*((sigma/dist)**12 - (sigma/dist)**6) - pot_rc  !medicion potencial
        end if
      end do
    end do

    deallocate(dr)

    return
  end subroutine Fuerzas

  subroutine Minimizacion()
    integer (kind = 4) :: i, j, k, row_r_max, row_r_min, col_r_max, col_r_min
    real (8) :: dt, EnPotant

    col_r_min = lbound(r, dim=2)
    col_r_max = ubound(r, dim=2)
    row_r_min = lbound(r, dim=1)
    row_r_max = ubound(r, dim=1)
    call EscrituraVTF("prueba.vtf", 0)
    call EscrituraVTF("prueba.vtf", 1)

    dt = 0.0005D0
    k=0
    EnPotant = 10.0D0

    do while (abs(EnPotant-EnPot) .gt. 1.0D-2 .or. Enpot .ge. 0.0D0) !Esta condicion puede ser controversial por que puede no cumplirse
      k=k+1                                                          !Quizas tambien habria que agregar que corte si se llega a una cantidad de
      EnPotant = EnPot                                               !pasos M.

      do j=col_r_min,col_r_max
        do i=row_r_min, row_r_max
          r(i, j) = Modulo(r(i, j) + 0.5D0 * f(i, j) * dt**2, L)     !La funcion MODULO(A,P) hace las condiciones periodicas
          !print *, r(i,j)
        end do
      end do

      call fuerzas()

      if (mod(k,500) .eq. 0) THEN
        print *, k, EnPot
        call EscrituraVTF("prueba.vtf", 1) !esto es para que grabe en el archivo VTF para VMD
      end if
    end do
    return
  end subroutine Minimizacion

  subroutine EscrituraVTF(filename, mode)
    implicit none
    integer (kind=4) :: mode, idf=100, j
    CHARACTER (Len=*)::filename
    logical::es

    select case(mode)
    case(0)
      !Inicializa archivo con el encabezado
      open(idf, file=trim(filename), status="REPLACE", action="write")
      write(6,*) "Inicializando archivo VTF"
      write(idf,*) "# STRUCTURE BLOCK"
      write(idf,*) "# define the default atom"
      write(idf,"(A5,I1,A1,I3,A13,F3.1,A7)") "atom ",0,":",N-1,"      radius ",sigma," name S"
      write(idf,*) "timestep"
      write(idf,*) " "
      close(idf)
    case(1)
      !agrega las distintas configuraciones
      inquire(file=filename, exist=es)
      IF(es) THEN
        open(idf, file=trim(filename), status="old", position="append", action="write")
        write(6,*) "Grabando datos en archivo VTF"
        do j=lbound(r, dim=2),ubound(r, dim=2)
          write(idf, *) r(:,j)
        end do
        write(idf,*) "timestep"
        write(idf,*) " "
        close(idf)
      else
        write(6,*) "No existe archivo VTF. Primero inicialicelo con mode=0"
      End if

    end select

    return
  end subroutine EscrituraVTF

  subroutine VelocIni()
    implicit NONE
    LOGICAL :: es
    integer (kind = 4) :: i, j, k

    inquire(file="v.dat", exist=es)

    IF (es .eqV. .FALSE.) THEN
      !Genera posiciones aleatorias
      print *, "Genero velocidades aleatorias..."
      do j=lbound(v, dim=2),ubound(v, dim=2)
        do i=lbound(v, dim=1), ubound(v, dim=1)
          v(i,j) = sqrt(1.5*epsil/masa)*rnor()
        end do
      end do

      !calculo velocidad del centro de masa
      v_cm = 0.0D0
      do i=lbound(v, dim=1), ubound(v, dim=1)
        v_cm(i) = sum(v(i,:))/float(N)
        v(i,:) = v(i,:) - v_cm(i)
      end do

      print *, "Velocidad CM: ", v_cm

    ELSE
      ! Lee posiciones de ARCHIVO
      print *, "Leyendo velocidades del archivo v.dat"
      open(10, file="v.dat", status="old",action="read")
      read(10,*) v
      close(10)

    END IF

  return
  end subroutine VelocIni

subroutine PosIni()
  implicit NONE
  integer (kind = 4) :: i, j, k
  logical:: es

  inquire(file="v.dat", exist=es)

  IF (es .EQV. .FALSE.) THEN
    !Genera posiciones aleatorias
    print *, "Genero posiciones aleatorias..."
    DO i=1,N
      do j=1, dim
        r(j,i) = L * uni()
      end do
    END DO

    print *, "Minimizando Energia potencial de la Configuración Inicial"
    call Minimizacion()

  else
     ! Lee posiciones de ARCHIVO
     print *, "Leyendo posiciones del archivo r.dat"
     open(10, file="r.dat", status="old",action="read")
     read(10,*) r
     close(10)
  end IF

return
end subroutine PosIni

subroutine Velocity_Verlet()
  implicit NONE
  integer (kind = 4) :: i, j, k
  REAL (kind = 8 ),dimension(:, :), allocatable :: v_medio
  real (kind =8 ):: r_aux, Nf


  do j= lbound(r, dim=2), ubound(r, dim=2)
    do i= lbound(r, dim=1), ubound(r, dim=1)
      r_aux = r(i,j) + v(i,j) * dt_MD + 0.5D0 * (f(i,j)/masa) * (dt_MD)**2
      r(i,j) = Modulo(r_aux, L)
      v(i,j) = v(i,j) + 0.5D0 * dt_MD * (f(i,j)/masa)
    End do
  End do

  call Fuerzas()
  call lgv_force()

  do j= lbound(r, dim=2), ubound(r, dim=2)
    do i= lbound(r, dim=1), ubound(r, dim=1)
      v(i,j) = v(i,j) + 0.5D0 * dt_MD  * (f(i,j)/masa)
    end do
  end do

  !CORRIJO LA VELOCIDAD PARA QUE LA VELOCIDAD DEL CENTRO DE MASA SEA CERO
  v_cm = 0.0D0
  do i=lbound(v, dim=1), ubound(v, dim=1)
    v_cm(i) = sum(v(i,:))/float(N)
    v(i,:) = v(i,:) - v_cm(i)
  end do

  !CALCULO LA ENERGIA CINETICA Y LA TEMPERATURA Y AGREGO EL TERMINO DE LA TEMPERATURA A LA PRESION
  EnCin = 0.0D0
  Tmed = 0.0D0
  Nf = 3.0D0 * N - 3.0D0
  do j= lbound(r, dim=2), ubound(r, dim=2)
    do i= lbound(r, dim=1), ubound(r, dim=1)
      EnCin = EnCin + (0.5D0)*masa*(v(i,j))**2
    end do
  end do

  Tmed = EnCin * 2 /(kb*Nf)
  Pmed = rho*kb*Tmed + Pv

  PromT = PromT + Tmed
  PromT2 = PromT2 + Tmed*Tmed
  PromP = PromP + Pmed
  PromP2 = PromP2 + Pmed*Pmed
Return

end subroutine Velocity_Verlet

subroutine lgv_force()
  implicit NONE
  integer(kind=4)::i,j
  real(kind = 8 ) :: Frand = 0, aleat
  real(kind=8),dimension(:,:), allocatable :: fvisc

  allocate(Fvisc(dim,N))
  Fvisc = -1.0D0 * gamma * v
  F = F + Fvisc

  do j=lbound(f, dim=2), ubound(f, dim=2)
    do i=lbound(f, dim=1),ubound(f, dim=1)
      Frand = dsqrt(2*Temp*gamma*kb/dt_MD) * rnor()
!      aleat = int(uni() + 0.5D0)
!      print *, "Num Aleat:", aleat, "Fuerza Estocastica:", Frand
      f(i,j) = f(i,j) + Frand
    end do
  end do


Return
end subroutine lgv_force

subroutine Medir()
  implicit none
  logical :: es

  !call EnergiaCinetica()
  EnTot = EnPot + EnCin

  inquire(file=instDatos, exist=es)
  IF(es) THEN
    open(100, file=instDatos, status="old", position="append", action="write")
  else
    open(100, file=instDatos, status="new", position="append", action="write")
  End if

  write(100, *) EnPot/float(N), EnCin/float(N), EnTot/float(N), Tmed, Pmed

  close(100)

return
end subroutine Medir

end module rutinasMD
