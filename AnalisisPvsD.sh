#!/bin/bash

	cd $HOME/SimComp/Dinamica/1.1_Temp2
	
	echo 'Analizando: ' 

        for dens in 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 0.55 0.6 0.65 0.7 0.75
	do
		cd ${dens}_dens
		#CALCULO EL PROMEDIO DE LAS ENERGIAS Y ENERGIAS^2 CON SUS RESPCTIVOS DESVIOS ESTANDAR
		awk -v dens=$dens 'NR>1 {
			sumP+=$3; 
			sumPP+=$4; 
			sumP2+=$3*$3; 
			sumPP2+=$4*$4;
			
		} END {
			term = NR-1;
			print dens, sumP/term, sqrt(sumP2/term-(sumP/term)^2), sumPP/term, sqrt(sumPP2/term-(sumPP/term)^2)
		
		}' promedios.dat >> ../PvsDensidad2.dat
	
		cd ../

	done
	
