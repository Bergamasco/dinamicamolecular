#!/bin/bash

	cd $HOME/SimComp/Dinamica/0.3_Dens
	
	echo 'Analizando: ' 

	for temp in 0.7 0.75 0.8 0.85 0.9 0.95 1.0 1.05 1.10 1.15 1.20 1.25 1.30 1.35 1.40
	do
		cd ${temp}_temp
		#CALCULO EL PROMEDIO DE LAS ENERGIAS Y ENERGIAS^2 CON SUS RESPCTIVOS DESVIOS ESTANDAR
		awk -v temp=$temp 'NR>1 {
			sumP+=$3; 
			sumPP+=$4; 
			sumP2+=$3*$3; 
			sumPP2+=$4*$4;
			
		} END {
			term = NR-1;
			print temp, sumP/term, sqrt(sumP2/term-(sumP/term)^2), sumPP/term, sqrt(sumPP2/term-(sumPP/term)^2)
		
		}' promedios.dat >> ../PvsTemperatura.dat
	
		cd ../

	done
	
